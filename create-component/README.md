# create-component

A utility cli for create React component files.

## Install

This tool require [deno](https://deno.land/#installation).

Install deno

```bash
curl -fsSL https://deno.land/x/install/install.sh | sh
```

Install create-component 

```bash 
deno install --allow-read  --allow-write --unstable -n create-component ./create-component.ts
```
or 

Install create-component remote
```bash 
deno install --allow-read  --allow-write --unstable -n create-component https://bitbucket.org/together_price/dev-tools/raw/fbc5df55b539db79d2fc47b636025718bd4d481c/create-component/create-component.ts
```

## Usage

Create a component

```
create-component Component
# exammple
create-component HelloWorld
```

Create a svg component using [material-ui SvgIcon](https://material-ui.com/components/icons/#svgicon)
```
create-component Component -t svg
# exammple
create-component HelloEmoji -t svg
```

The name of the component must be PascalCase.

---
