#!/bin/env/sh deno install --allow-read  --allow-write --unstable -n create-component ./create-component.ts
import { parse } from "https://deno.land/std/flags/mod.ts";
import { emptyDir } from "https://deno.land/std/fs/mod.ts";

const defaultCode = (name: string) => `import React from 'react';

export type ${name}Props = {};
const ${name}: React.FC<${name}Props> = (props) => {
  return <></>
}

export default ${name};
`;

const defaultCodeSvg = (name: string) => `import React from 'react';
import { SvgIcon, SvgIconProps } from '@material-ui/core';

export type ${name}Props = Omit<SvgIconProps, 'viewBox'>;
const ${name}: React.FC<${name}Props> = (props) => {
  return (
    <SvgIcon {...props} viewBox="0 0 40 40">
    </SvgIcon>
  );
}

export default ${name};
`;
type Template = "svg" | "default";
const getTemplate = (template: Template, name: string) => {
  switch (template) {
    case "svg":
      return defaultCodeSvg(name);
    default:
      return defaultCode(name);
  }
};
const createComponent = async () => {
  const {
    _: [name],
    t = "default",
  } = parse(Deno.args);
  if (!name) {
    console.log("missing required argument [name]");
    console.log("usage create-component [name]");
    return;
  }
  await emptyDir(`./${name}`);
  await Deno.writeTextFile(
    `./${name}/index.tsx`,
    `export { default } from './${name}'`
  );
  await Deno.writeTextFile(
    `./${name}/${name}.tsx`,
    getTemplate(t as Template, name as string)
  );
  console.log(`component ${name} created!`);
};

await createComponent();
